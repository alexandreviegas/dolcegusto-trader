Rails.application.routes.draw do

  root 'trades#index'

  get '/login.html', :controller => 'login', :action => :new, :as => :login
  post '/authenticate.html', :controller => 'login', :action => :create, :as => :authenticate
  delete '/logout.html', :controller => 'login', :action => :destroy, :as => :logout

  resources :trades
  resources :flavours

  get 'trades/:flavour_id/new', controller: :trades, action: :new, as: :trade_flavour

end
