# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

flavours = [
  ['Ristretto Ardenza', 'ristretto-ardenza', 16, 1],
  ['Caffè Buongiorno', 'cafe-buongiorno', 16, 2],
  ['Café Au Lait', 'cafe-au-lait', 16, 3],
  ['Cortado', 'espresso-macchiato', 16, 1],
  ['Mocha', 'mocha', 8, 3],
  ['Vanilla Latte Macchiato', 'vanilla-latte-macchiato', 8, 3],
  ['Chococino', 'chococino', 8, 3],
  ['Nestea Pêssego ',  'nestea-peach-use-for-test',16, 3],
  ['Nestea Limão', 'nestea-lemon', 16, 3],
  ['Chai Tea Latte', 'chai-tea-latte', 8, 3],
  ['Marrakesh Style Tea', 'marrakesh-style-tea', 16, 3],
  ['Caramel Latte Macchiato', 'caramel-latte-macchiato', 8, 3],
  ['Latte Macchiato', 'latte-macchiato', 8, 3],
  ['Nescau', 'nescau', 16, 2],
  ['Cappuccino', 'cappuccino', 8, 3],
  ['Lungo', 'lungo', 16, 2],
  ['Espresso Barista', 'espresso-barista', 16, 1],
  ['Espresso Descafeinado', 'espresso-descafeinado', 16, 1],
  ['Espresso', 'espresso', 16, 1],
  ['Espresso Intenso', 'espresso-intenso', 16, 1],
  ['Choco Caramel', 'choco-caramel', 8, 3]
]

flavours.each do |flavour|
  name = flavour[0]
  url = flavour[1]
  amount = flavour[2]
  size = flavour[3]

  Flavour.create(name: name, url: url, amount: amount, size: size)
end
