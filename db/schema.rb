# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150811194533) do

  create_table "flavours", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.integer  "amount"
    t.integer  "size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trades", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "flavour_id"
    t.string   "amount"
    t.text     "changes_for"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "trades", ["flavour_id"], name: "index_trades_on_flavour_id"
  add_index "trades", ["user_id"], name: "index_trades_on_user_id"

  create_table "users", force: :cascade do |t|
    t.integer  "cpf",          limit: 8, null: false
    t.string   "name",                   null: false
    t.string   "registration",           null: false
    t.string   "email",                  null: false
    t.string   "phone_number",           null: false
    t.string   "area"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["area"], name: "index_users_on_area"
  add_index "users", ["cpf"], name: "index_users_on_cpf"
  add_index "users", ["email"], name: "index_users_on_email"
  add_index "users", ["name"], name: "index_users_on_name"
  add_index "users", ["phone_number"], name: "index_users_on_phone_number"
  add_index "users", ["registration"], name: "index_users_on_registration"

end
