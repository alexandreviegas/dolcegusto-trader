class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.integer :cpf, limit: 8, null: false
      t.string :name, null: false
      t.string :registration, null: false
      t.string :email, null: false
      t.string :phone_number, null: false
      t.string :area

      t.timestamps
    end

    add_index :users, :cpf
    add_index :users, :name
    add_index :users, :registration
    add_index :users, :email
    add_index :users, :phone_number
    add_index :users, :area

  end
end
