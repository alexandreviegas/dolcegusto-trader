class CreateTrades < ActiveRecord::Migration
  def change
    create_table :trades do |t|
      t.references :user, index: true, foreign_key: true
      t.references :flavour, index: true, foreign_key: true
      t.string :amount
      t.text :changes_for
      t.integer :status

      t.timestamps null: false
    end
  end
end
