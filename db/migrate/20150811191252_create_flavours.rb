class CreateFlavours < ActiveRecord::Migration
  def change
    create_table :flavours do |t|
      t.string :name
      t.string :url
      t.integer :amount
      t.integer :size

      t.timestamps null: false
    end
  end
end
