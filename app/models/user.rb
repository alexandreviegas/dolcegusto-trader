class User < ActiveRecord::Base
  include LDAP

  has_many :trades

  def self.authenticate(cpf, senha)

    # Inicializa as varíáveis com nil
    user = nil
    ldap_connection = nil

    # Recupera o DN do usuário que deseja fazer login
    dn = LDAP.get_user_dn(cpf)

    # Se o DN foi recuperado corretamente (CPF válido e existente no domínio), tenta estabelecer conexão com o DN obtido e a senha informada pelo usuário que deseja fazer login
    if dn
      ldap_connection = LDAP.ldap_connect(dn, senha)
    end

    # Se conseguiu logar corretamente com o DN e senha do usuário, prossegue e recupera dados do usuário no domínio
    if ldap_connection

      ldap_entry = LDAP.ldap_get_user(ldap_connection, cpf)

      if ldap_entry

        attributes = {:cpf => ldap_entry.uid[0], :name => ldap_entry.cn[0], :registration => ldap_entry.employeenumber[0],
                    :email => ldap_entry.mail[0], :phone_number => ldap_entry.telephonenumber[0], :area => ldap_entry.ou[0]}

        user = User.find_by_cpf(attributes[:cpf])

        #Caso o usuário ainda não exista na tabela, cadastra seus dados no sistema
        if user.nil?
          user = User.create(attributes)
        end

      end

    end

    return user
  end

end
