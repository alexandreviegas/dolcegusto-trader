require 'xmpp4r'
require 'xmpp4r/client'
include Jabber
class Messenger

  attr_reader :client

  def initialize
    @jabber_id = ENV['JID']
    @jabber_password = ENV['JID_PASSWORD']
    @jabber_server = ENV['JABBER_SERVER']
    @jabber_port = ENV['JABBER_PORT']
  end

  def connect
    @client = Client.new(JID.new(@jabber_id))
    @client.use_ssl = true
    @client.connect(@jabber_server, @jabber_port)
    @client.auth @jabber_password
  end

  def send_message(to, body)
    subject = 'Troca de Cápuslas Dolce Gusto'
    message = Message.new(to, body).set_type(:normal).set_subject(subject)
    @client.send message
  end

  def close
    @client.close
  end

end
