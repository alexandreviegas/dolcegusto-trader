class Trade < ActiveRecord::Base

  belongs_to :user
  belongs_to :flavour

  store :changes_for, accessors: [ :flavour_ids ], coder: JSON

  validates_presence_of :user_id, :flavour_id, :amount, :changes_for

  validates :amount, numericality: { only_integer: true, greather_than: 0 }

  after_save :send_message_to_traders

  def send_message_to_traders

    trades = Trade.where(flavour_id: self.flavour_ids.map{ |id| id.to_i}).where.not(id: self.id)
    if !trades.empty?
      messenger = Messenger.new
      messenger.connect

      trades.each do |trade|
        if trade.flavour_ids.map{ |id| id.to_i }.include?(self.flavour_id)
          msg = "#{trade.user.name} quer trocar #{pluralize( trade.amount, 'cápsula' )} de #{trade.flavour.name} que você deseja por cápsulas de #{self.flavour.name} que você possui."
          messenger.send_message(self.user.email.gsub('serpro.gov.br', 'sim.serpro.gov.br'), msg)

          msg = "#{self.user.name} quer trocar #{pluralize( self.amount, 'cápsula' )} de #{self.flavour.name} que você deseja por cápsulas de #{trade.flavour.name} que você possui."
          messenger.send_message(trade.user.email.gsub('serpro.gov.br', 'sim.serpro.gov.br'), msg)

        end
        messenger.close
      end

    end

  rescue => e
    Rails.logger.error { "#{e.message} #{e.backtrace.join("\n")}" }
  end

end
