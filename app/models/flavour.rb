class Flavour < ActiveRecord::Base
  enum size: { small: 1, medium: 2, large: 3 }

  default_scope { order(:name) }
end
