class LoginController < ApplicationController

  skip_before_filter :authenticate!, :only => [:new, :create]

  def new
  end

  def create
    cpf = unmask_number(login_params[:cpf], '999.999.999-99')
    user = User.authenticate(cpf, login_params[:password])

    if user
      session[:user_id] = user.id
      redirect_to root_url
    else
      session[:user_id] = nil
      flash.now.alert = 'Erro na autenticação'
      render :new
    end

  end

  def destroy
    session[:user_id] = nil
    current_user = nil
    redirect_to root_url
  end

  private
    def login_params
      params.require(:login).permit(:cpf, :password)
    end

    def unmask_number(number, mask)
      if number != nil and !number.empty?
        length = (mask.gsub /[^0-9]/, "").length
        number = number.gsub /[^0-9]/, ""
        number = number.rjust length, '0'
      end
    end

end
