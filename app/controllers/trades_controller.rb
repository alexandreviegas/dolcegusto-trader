class TradesController < ApplicationController

  before_action :set_trade, only: [:edit, :update, :destroy]

  def index
    @trades = Trade.all
  end

  def new
    @trade = Trade.new(flavour_id: params[:flavour_id])
    @flavour = Flavour.find(params[:flavour_id])
    @flavours = Flavour.all.where.not(id: @flavour.id)
  end

  def create
    @trade = Trade.new(trade_params)
    @trade.user_id = current_user.id
    if @trade.save
      redirect_to trades_url, notice: 'Troca cadastrada com sucesso.'
    else
      @flavour = Flavour.find(params[:trade][:flavour_id])
      @flavours = Flavour.all.where.not(id: @flavour.id)
      render :new
    end
  end

  def edit
    @flavour = @trade.flavour
    @flavours = Flavour.all.where.not(id: @flavour.id)
  end

  def update

    if @trade.update(trade_params)
      redirect_to trades_url, notice: 'Troca atualizada com sucesso.'
    else
      @flavours = Flavour.all
      render :edit
    end
  end

  def destroy
    if @trade.user_id == current_user.id
      @trade.destroy
      message = 'Troca excluída com sucesso.'
    else
      message = 'Você não tem permissão para excluir essa troca.'
    end
    redirect_to trades_url, notice: message
  end

  private
    def trade_params
      params.require(:trade).permit(:flavour_id, :amount, flavour_ids: [] )
    end

    def set_trade
      @trade = Trade.find(params[:id])
    end
end
