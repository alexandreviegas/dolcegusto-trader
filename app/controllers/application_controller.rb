class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate!

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  helper_method :current_user

  private
  def authenticated?
    !current_user.nil?
  end
  helper_method :authenticated?

  def authenticate!
    unless authenticated?
      session[:ref] = request.url
      redirect_to login_url, :alert => 'Você precisa estar autenticado para acessar esta página.'
    end
  end

end
