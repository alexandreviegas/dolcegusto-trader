# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  flavour_image = $('.flavour-image')
  if flavour_image.length
    flavour_image.on 'click', ->
      id = $(this).data('flavour-id')
      $checkbox = $('#checkbox-flavour-id-'+id)
      if $checkbox.length
        if $checkbox.is(':checked')
          $checkbox.prop('checked', false)
        else
          $checkbox.prop('checked', true)
