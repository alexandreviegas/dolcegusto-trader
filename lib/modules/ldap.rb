# require 'net/ldap'
module LDAP

  def self.ldap_connect(dn, senha)

    host = Rails.configuration.x.ldap_serpro.host
    treebase = Rails.configuration.x.ldap_serpro.treebase

    ldap = Net::LDAP.new :host => host, :port => 389, :auth => {:method => :simple, :username => dn, :password => senha}, :base => treebase

    if ldap.bind
      puts 'autenticado'
      ldap
    else
      puts 'falha de autenticacao'
      false
    end
  end


  def self.ldap_get_user(ldap_connection, cpf)
    filter = Net::LDAP::Filter.eq("uid", cpf)
    result = ldap_connection.search(:filter => filter)
    result[0]
  end


  def self.get_user_dn(cpf)
    dn = Rails.configuration.x.ldap_serpro.dn
    host = Rails.configuration.x.ldap_serpro.host
    treebase = Rails.configuration.x.ldap_serpro.treebase
    ldap = Net::LDAP.new :host => host, :port => 389, :auth => {:method => :simple, :username => dn, :password => "webdesdr"}, :base => treebase

    filter = Net::LDAP::Filter.eq("uid", cpf)
    result = ldap.search(:filter => filter)

    if result[0]
      result[0].dn
    end

  end

end
